from django.db import models
from django.conf import settings


USER_MODEL = settings.AUTH_USER_MODEL


class Project(models.Model):
    class Priorities(models.TextChoices):
        LOW = "Low", "Low"
        MEDIUM = "Medium", "Medium"
        HIGH = "HIGH", "HIGH"

    # PRIORITIES = [
    #     (LOW, "Low"),
    #     (MEDIUM, "Medium"),
    #     (HIGH, "HIGH"),
    # ]
    name = models.CharField(max_length=200)
    description = models.TextField()
    members = models.ManyToManyField(USER_MODEL, related_name="projects")
    priority = models.CharField(
        max_length=10,
        choices=Priorities.choices,
        default=Priorities.LOW,
        blank=Priorities.LOW,
    )

    def __str__(self):
        return self.name
