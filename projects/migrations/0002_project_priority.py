# Generated by Django 4.1.1 on 2022-09-20 17:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("projects", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="project",
            name="priority",
            field=models.CharField(
                choices=[(0, "Low"), (1, "Medium"), (2, "HIGH")],
                default=0,
                max_length=10,
            ),
        ),
    ]
